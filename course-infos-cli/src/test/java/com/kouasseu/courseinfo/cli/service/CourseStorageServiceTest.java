package com.kouasseu.courseinfo.cli.service;

import com.kouasseu.courseinfo.domain.PojoCourses;
import com.kouasseu.courseinfo.orm.EntityManager;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  09/07/2023 -- 21:24<br></br>
 * By : @author alexk<br></br>
 * Project : course-info<br></br>
 * Package : com.kouasseu.courseinfo.cli.service<br></br>
 */
@DisplayName("Course Storage Service Should")
class CourseStorageServiceTest {

    @DisplayName("persist pluralsight course in database")
    @Test
    void persistCourseInDatabase() throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        EntityManager<PojoCourses> entityManager = new InMemoryEntityManager();
        CourseStorageService service = new CourseStorageService(entityManager);

        PluralSightCourse ps1 = new PluralSightCourse("1", "Title 1",
                "01:40:00.123", "/url-1", "2020-03-17T00:00:00+00:00", false);

        service.storePluralsightCourses(List.of(ps1));

        PojoCourses expected = new PojoCourses("1", "Title 1", 100, "2020-03-17T00:00:00+00:00", "https://app.pluralsight.com/url-1");
        assertEquals(List.of(expected).size(), entityManager.findAll(PojoCourses.class).size()); //assertEquals or assertSame don't work because of using Pojo, which are different in memory

    }

    static class InMemoryEntityManager implements EntityManager<PojoCourses> {

        private final List<PojoCourses> courses = new ArrayList<>();

        @Override
        public void persist(PojoCourses course)  {
            courses.add(course);
        }

        @Override
        public void update(PojoCourses pojoCourses) {
            throw new UnsupportedOperationException();
        }

        @Override
        public PojoCourses find(Class<PojoCourses> clss, Object primaryKey) {
            return courses.stream().filter(course1 -> course1.getId().equals(primaryKey)).findFirst().orElseThrow();
        }

        @Override
        public List<PojoCourses> findAll(Class<PojoCourses> clss)  {
            return courses;
        }
    }
}