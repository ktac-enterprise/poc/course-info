package com.kouasseu.courseinfo.cli.service;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  09/07/2023 -- 06:57<br></br>
 * By : @author alexk<br></br>
 * Project : course-info<br></br>
 * Package : com.kouasseu.courseinfo.cli.service<br></br>
 */

@DisplayName("Pluralsight Course should")
class PluralSightCourseTest {

    @ParameterizedTest
//    @CsvSource({"00:05:08, 5", "01:08:26.0698110, 68", "00:00:00, 0"})
    @CsvSource(textBlock = """
                00:05:08, 5
                01:08:26.0698110, 68
                00:00:00, 0
            """)
    void durationShouldHaveCorrectValueInMinutes(String input, long expected) {
        PluralSightCourse course =
                new PluralSightCourse("id", "Test course", input, "url", "2020-03-17T00:00:00+00:00", false);

        assertEquals(expected, course.durationInMinutes());
    }
}