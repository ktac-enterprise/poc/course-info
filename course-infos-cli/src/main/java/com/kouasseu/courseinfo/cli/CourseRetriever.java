package com.kouasseu.courseinfo.cli;

import com.kouasseu.courseinfo.cli.service.CourseRetrievalService;
import com.kouasseu.courseinfo.cli.service.CourseStorageService;
import com.kouasseu.courseinfo.cli.service.PluralSightCourse;
import com.kouasseu.courseinfo.domain.PojoCourses;
import com.kouasseu.courseinfo.orm.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import static java.util.function.Predicate.not;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  08/07/2023 -- 07:32<br></br>
 * By : @author alexk<br></br>
 * Project : course-info<br></br>
 * Package : org.kouasseu.courseinfo.cli<br></br>
 */
public class CourseRetriever {
    private static final Logger LOG = LoggerFactory.getLogger(CourseRetriever.class);
    public static void main(String[] args) {
        LOG.info("CourseRetriever starting !");
        
        if(args.length == 0) {
            LOG.warn("Please provide an author name as first argument.");
            return;
        }

        try {
            retrieveCourses(args[0]);
        } catch (Exception e) {
            LOG.error("Unexpected error !", e);
        }
    }

    private static void retrieveCourses(String authorId) {
        LOG.info("Retrieving courses for author '{}'", authorId);
        CourseRetrievalService courseRetrievalService = new CourseRetrievalService();
        EntityManager<PojoCourses> coursesEntityManager = EntityManager.getCourseInstance();
        CourseStorageService service = new CourseStorageService(coursesEntityManager);



        List<PluralSightCourse> coursesToStore = courseRetrievalService.getCoursesFor(authorId)
                .stream()
                .filter(not(PluralSightCourse::isRetired))
                .toList();
        LOG.info("Retrieving the following {} courses {}", coursesToStore.size(), coursesToStore);

        service.storePluralsightCourses(coursesToStore);

        LOG.info("Courses successfully stored");
    }
}
