package com.kouasseu.courseinfo.cli.service;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.time.Duration;
import java.time.LocalTime;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  08/07/2023 -- 08:28<br></br>
 * By : @author alexk<br></br>
 * Project : course-info<br></br>
 * Package : org.kouasseu.courseinfo.cli.service<br></br>
 */

@JsonIgnoreProperties(ignoreUnknown = true) //Si on veut faire un parsing partiel des informations qu'on recoit en entrée et ceux qu'on veut obtenir en sortie, on dire à Json d'ingnorer certains attributs avec cette Annotation
public record PluralSightCourse(String id, String title, String duration, String contentUrl, String displayDate, boolean isRetired) {
    // duration = "00:05:37"

    public long durationInMinutes() {
        return Duration.between(
                LocalTime.MIN,
                LocalTime.parse(duration())
        ).toMinutes();
    }
}
