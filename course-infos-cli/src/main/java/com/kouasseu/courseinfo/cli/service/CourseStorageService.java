package com.kouasseu.courseinfo.cli.service;

import com.kouasseu.courseinfo.beanmanager.BeanManager;
import com.kouasseu.courseinfo.domain.PojoCourses;
import com.kouasseu.courseinfo.orm.EntityManager;

import java.sql.SQLException;
import java.util.List;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  09/07/2023 -- 21:09<br></br>
 * By : @author alexk<br></br>
 * Project : course-info<br></br>
 * Package : com.kouasseu.courseinfo.cli.service<br></br>
 */
public class CourseStorageService {
    private static final String PS_BASE_URL = "https://app.pluralsight.com";
    private final EntityManager<PojoCourses> coursesEntityManager;

    public CourseStorageService(EntityManager<PojoCourses> coursesEntityManager) {
        this.coursesEntityManager = coursesEntityManager;
    }

    public void storePluralsightCourses(List<PluralSightCourse> psCourses) {
        for (PluralSightCourse psCourse: psCourses) {
            PojoCourses course = new PojoCourses(psCourse.id(),
                    psCourse.title(),
                    psCourse.durationInMinutes(),
                    psCourse.displayDate(),
                    PS_BASE_URL + psCourse.contentUrl());
            try {
                coursesEntityManager.persist(course);
            } catch (SQLException | IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
