package com.kouasseu.courseinfos.server;

import com.kouasseu.courseinfo.domain.PojoCourses;
import com.kouasseu.courseinfo.orm.EntityManager;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.bridge.SLF4JBridgeHandler;

import java.net.URI;
import java.util.logging.LogManager;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  10/07/2023 -- 22:07<br></br>
 * By : @author alexk<br></br>
 * Project : course-info<br></br>
 * Package : com.kouasseu.courseinfos.server<br></br>
 */
public class CourseServer {
    private static final Logger LOG = LoggerFactory.getLogger(CourseServer.class);
    private static final String BASE_URI = "http://localhost:8080/";


    static {
        //Redirection des log géré par le JDK Log vers SL4J
        LogManager.getLogManager().reset();
        SLF4JBridgeHandler.install();
    }
    public static void main(String[] args) {
        LOG.info("Starting HTTP server...");

        // Creation d'une configuration de resource où nous souscrivons notre classe CourseResource qui expose notre Endpoint
        EntityManager<PojoCourses> entityManager = EntityManager.getCourseInstance();
        ResourceConfig config = new ResourceConfig().register(new CourseResource(entityManager));

        //Creation du serveur HTTP
        GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URI), config);
    }
}
