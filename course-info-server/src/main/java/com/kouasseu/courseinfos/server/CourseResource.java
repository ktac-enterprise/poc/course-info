package com.kouasseu.courseinfos.server;

import com.kouasseu.courseinfo.domain.PojoCourses;
import com.kouasseu.courseinfo.orm.EntityManager;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.stream.Stream;


/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  09/07/2023 -- 22:50<br></br>
 * By : @author alexk<br></br>
 * Project : course-info<br></br>
 * Package : com.kouasseu.courseinfos.server<br></br>
 */
@Path("/courses")
public class CourseResource {
    private static final Logger LOG = LoggerFactory.getLogger(CourseResource.class);

    private final EntityManager<PojoCourses> entityManager;

    public CourseResource(EntityManager<PojoCourses> entityManager) {
        this.entityManager = entityManager;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Stream getCourses(){
        try {
            return entityManager.findAll(PojoCourses.class)
                    .stream()
                    .sorted(Comparator.comparing(PojoCourses::getId));
        } catch (Exception e) {
            LOG.error("Could not retrieve courses from the database", e);
            throw new NotFoundException();
        }
    }

    @POST
    @Path("/{id}/notes")
    @Consumes(MediaType.TEXT_PLAIN)
    public void addNotes(@PathParam("id") String id, String notes)  {
        try {
            PojoCourses courses = entityManager.find(PojoCourses.class, id);
            courses.setNote(notes);
            entityManager.update(courses);
            LOG.info("Course updated !!!");
        } catch (SQLException | InvocationTargetException | NoSuchMethodException | InstantiationException | IllegalAccessException e) {
            LOG.error("Could not found a course to update with the given id: {}", id, e);
            throw new NotFoundException(e);
        }
    }
}
