package com.kouasseu.courseinfo.domain;

import com.kouasseu.courseinfo.annotations.Column;
import com.kouasseu.courseinfo.annotations.PrimaryKey;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Metamodel {


    private final Class<?> clss;

    public static Metamodel of(Class<?> clss) {
        return new Metamodel(clss);
    }

    public  Metamodel(Class<?> clss) {
        this.clss = clss;
    }

    public PrimaryKeyField getPrimaryKey() {
        Field[] fields = this.clss.getDeclaredFields();
        for (Field field: fields) {
            PrimaryKey primaryKey = field.getAnnotation(PrimaryKey.class);
            if (primaryKey != null) {
                return new PrimaryKeyField(field);
            }
        }
        throw  new IllegalArgumentException("No primary key found in class : " +this.clss.getSimpleName()) ;
    }

    public List<ColumnField> getColumns() {
        List<ColumnField> columnFields = new ArrayList<>();
        Field[] fields = this.clss.getDeclaredFields();
        for (Field field: fields) {
            if (field.getAnnotation(Column.class) != null) {
                ColumnField columnField = new ColumnField(field);
                columnFields.add(columnField);
            }
        }
        return columnFields;
    }

    public String buildInsertRequest() {
        // INSERT INTO Person(id, name, age) value (?, ?, ?)
        return """
                INSERT INTO %s
                (%s) values (%s)
                """.formatted(this.clss.getSimpleName(), buildColumnNames(), buildQuestionMarksElement());
    }

    public String buildSelectRequest() {
        //SELECT id, name, age from Person WHERE id = ?
        return """
                SELECT %s
                FROM %s
                WHERE %s = ?
                """.formatted(buildColumnNames(), this.clss.getSimpleName(), getPrimaryKey().getName());
    }

    public String buildSelectAllRequest() {
        //SELECT id, name, length, displayDate, url FROM Course
        return """
                SELECT %s
                FROM %s
                """.formatted(buildColumnNames(), this.clss.getSimpleName());
    }

    public String buildUpdateRequest() {
        return """
                UPDATE %s
                SET %s
                WHERE %s = ?
                """.formatted(this.clss.getSimpleName(), buildColumnForSetUpdate(), getPrimaryKey().getName());
    }

    private String buildQuestionMarksElement() {
        int numberOfColumns = getColumns().size() + 1;
        return IntStream.range(0, numberOfColumns).mapToObj(index -> "?").collect(Collectors.joining(", "));
    }

    private String buildColumnNames() {
        String primaryKeyColumnName = getPrimaryKey().getName();
        List<String> columnNames = getColumns().stream().map(ColumnField::getName).collect(Collectors.toList());
        columnNames.add(0, primaryKeyColumnName);
        return String.join(", ", columnNames);
    }

    private String buildColumnForSetUpdate() {
        return getColumns().stream().map(ColumnField::getName).collect(Collectors.joining(" = ?, ", "", " = ?"));
    }


}
