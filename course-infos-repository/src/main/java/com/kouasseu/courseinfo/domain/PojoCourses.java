package com.kouasseu.courseinfo.domain;

import com.kouasseu.courseinfo.annotations.Column;
import com.kouasseu.courseinfo.annotations.PrimaryKey;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  09/07/2023 -- 21:59<br></br>
 * By : @author alexk<br></br>
 * Project : course-info<br></br>
 * Package : com.kouasseu.courseinfo.domain<br></br>
 */
public class PojoCourses {
    @PrimaryKey(name = "id")
    private String id;
    @Column(name = "c_name")
    private String name;
    @Column(name = "c_length")
    private long length;
    @Column(name = "c_display_date")
    private String displayDate;
    @Column(name = "c_url")
    private String url;

    @Column(name = "c_note")
    private String note;

    public PojoCourses() {
    }

    public PojoCourses(String id, String name, long length, String displayDate, String url) {
        this.id = id;
        this.name = name;
        this.length = length;
        this.displayDate = displayDate;
        this.url = url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getLength() {
        return length;
    }

    public void setLength(long length) {
        this.length = length;
    }

    public String getDisplayDate() {
        return displayDate;
    }

    public void setDisplayDate(String displayDate) {
        this.displayDate = displayDate;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public String toString() {
        return "PojoCourses{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", length=" + length +
                ", displayDate='" + displayDate + '\'' +
                ", url='" + url + '\'' +
                ", note='" + note + '\'' +
                '}';
    }
}
