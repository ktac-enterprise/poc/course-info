package com.kouasseu.courseinfo.domain;

import com.kouasseu.courseinfo.annotations.Column;
import com.kouasseu.courseinfo.annotations.PrimaryKey;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  09/07/2023 -- 19:02<br></br>
 * By : @author alexk<br></br>
 * Project : course-info<br></br>
 * Package : com.kouasseu.courseinfo.domain<br></br>
 */

public record Courses(@PrimaryKey(name = "id") String id,
                      @Column(name = "c_name") String name,
                      @Column(name = "c_length") long length,
                      @Column(name = "c_display_name") String displayDate,
                      @Column(name = "c_url") String url) {
    public Courses { //compact constructor
        filled(id);
        filled(name);
        filled(url);
    }

    private static void filled(String s) {
        if( s == null || s.isBlank()) {
            throw new IllegalArgumentException("No value present !");
        }
    }
}
