package com.kouasseu.courseinfo.domain;

import com.kouasseu.courseinfo.annotations.PrimaryKey;

import java.lang.reflect.Field;

public class PrimaryKeyField {
    private final Field field;
    private final PrimaryKey primaryKey;

    public PrimaryKeyField(Field field) {
        this.field = field;
        this.primaryKey = field.getAnnotation(PrimaryKey.class);
    }

    public Class<?> getType() {
        return field.getType();
    }

    public String getName() {
        return primaryKey.name();
    }

    public Object getValue(Object o) throws IllegalAccessException {
        field.setAccessible(true);
        return field.get(o);
    }

    public Field getFiled() {
        return this.field;
    }
}
