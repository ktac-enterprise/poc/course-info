package com.kouasseu.courseinfo.config;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  11/07/2023 -- 08:13<br></br>
 * By : @author alexk<br></br>
 * Project : course-info<br></br>
 * Package : com.kouasseu.courseinfo.config<br></br>
 */
public record RepositoryProperties (String url, String username, String password) {
}
