package com.kouasseu.courseinfo.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  29/05/2023 -- 22:42<br></br>
 * By : @author alexk<br></br>
 * Project : Java Tips from 8 to 20<br></br>
 * Package : com.kouasseu.annotations<br></br>
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Inject {
}
