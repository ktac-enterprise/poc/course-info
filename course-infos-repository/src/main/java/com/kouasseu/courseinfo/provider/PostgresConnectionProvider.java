package com.kouasseu.courseinfo.provider;

import com.kouasseu.courseinfo.annotations.Provides;
import com.kouasseu.courseinfo.config.RepositoryProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  29/05/2023 -- 22:43<br></br>
 * By : @author alexk<br></br>
 * Project : Java Tips from 8 to 20<br></br>
 * Package : com.kouasseu.provider<br></br>
 */
public class PostgresConnectionProvider {
    private static final Logger LOG = LoggerFactory.getLogger(PostgresConnectionProvider.class);

    @Provides
    public Connection buildConnection() throws SQLException {
        RepositoryProperties properties = loadDatabaseProperties();
        LOG.info("Repository properties loaded !");
        Connection connection = DriverManager.getConnection(properties.url(),  properties.username(), properties.password());
        String INIT_TABLE_COURSES = """
                    CREATE TABLE IF NOT EXISTS POJOCOURSES (
                        ID VARCHAR PRIMARY KEY NOT NULL,
                        C_NAME VARCHAR NOT NULL,
                        C_LENGTH VARCHAR NOT NULL,
                        C_DISPLAY_DATE VARCHAR,
                        C_URL VARCHAR NOT NULL,
                        C_NOTE VARCHAR
                    );
                """;
        connection.createStatement().execute(INIT_TABLE_COURSES);
        return connection;
    }

    private static RepositoryProperties loadDatabaseProperties() {
        try (InputStream propertiesStream = PostgresConnectionProvider.class.getResourceAsStream("/repository.properties")) {
            Properties properties = new Properties();
            properties.load(propertiesStream);
            return new RepositoryProperties(properties.getProperty("course-infos.database.url"),
                    properties.getProperty("course-infos.database.username"),
                    properties.getProperty("course-infos.database.password"));
        } catch (IOException e) {
            throw new IllegalArgumentException("Could not load database filename");
        }
    }
}
