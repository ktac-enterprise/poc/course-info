package com.kouasseu.courseinfo.orm;

import com.kouasseu.courseinfo.beanmanager.BeanManager;
import com.kouasseu.courseinfo.domain.PojoCourses;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.List;

public interface EntityManager<T> {
    void persist(T t) throws SQLException, IllegalAccessException;

    void update(T t) throws SQLException, IllegalAccessException;

    T find(Class<T> clss, Object primaryKey) throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException;

    List<T> findAll(Class<T> clss) throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException;

    static EntityManager<PojoCourses> getCourseInstance() {
        return BeanManager.getInstance().getInstance(ManagedEntityManager.class);
    }
}
