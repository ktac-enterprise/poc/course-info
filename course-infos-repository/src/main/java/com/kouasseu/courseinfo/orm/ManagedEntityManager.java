package com.kouasseu.courseinfo.orm;

import com.kouasseu.courseinfo.annotations.Inject;
import com.kouasseu.courseinfo.domain.ColumnField;
import com.kouasseu.courseinfo.domain.Metamodel;
import org.apache.commons.lang3.RandomStringUtils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

public class ManagedEntityManager<T> implements EntityManager<T> {

    AtomicLong idGenerator = new AtomicLong(0L);
    @Inject
    private Connection connection;
    @Override
    public void persist(T t) throws SQLException, IllegalAccessException {

        Metamodel metamodel = Metamodel.of(t.getClass());
        String sql = metamodel.buildInsertRequest();

        PreparedStatement statement = prepareStatementWith(sql).andParameters(t);
        statement.executeUpdate();

    }

    @Override
    public void update(T t) throws SQLException, IllegalAccessException {
        Metamodel metamodel = Metamodel.of(t.getClass());
        String sql = metamodel.buildUpdateRequest();

        PreparedStatement statement = prepareStatementWith(sql).andParametersForUpdate(t);
        statement.executeUpdate();
    }

    @Override
    public T find(Class<T> clss, Object primaryKey) throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        Metamodel metamodel = Metamodel.of(clss);
        String sql = metamodel.buildSelectRequest();
        PreparedStatement statement = prepareStatementWith(sql).andPrimaryKey(primaryKey);
        ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        return buildInstanceFrom(clss, resultSet);
    }

    @Override
    public List<T> findAll(Class<T> clss) throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        Metamodel metamodel = Metamodel.of(clss);
        String sql = metamodel.buildSelectAllRequest();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(sql);
        List<T> result = new ArrayList<>();
        while (resultSet.next()) {
            result.add(buildInstanceFrom(clss, resultSet));
        }
        return result;
    }

    private PreparedStatementWrapper prepareStatementWith(String sql) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        return new PreparedStatementWrapper(preparedStatement);
    }

    private class PreparedStatementWrapper {

        private final PreparedStatement statement;

        public PreparedStatementWrapper(PreparedStatement statement) {
            this.statement = statement;
        }

        public PreparedStatement andParameters(T t) throws SQLException, IllegalAccessException {
            Metamodel metamodel = Metamodel.of(t.getClass());
            Class<?> primaryKeyType = metamodel.getPrimaryKey().getType();

            if(primaryKeyType == long.class) {
                long id = idGenerator.incrementAndGet();
                statement.setLong(1, id);
                Field field = metamodel.getPrimaryKey().getFiled();
                field.setAccessible(true);
                field.set(t, id);
            }


            if(primaryKeyType == String.class) {
                String id = RandomStringUtils.randomAlphabetic(8);
                statement.setString(1, id);
                Field field = metamodel.getPrimaryKey().getFiled();
                field.setAccessible(true);
                field.set(t, id);
            }

            for (int columnIndex = 0; columnIndex < metamodel.getColumns().size(); columnIndex ++) {
                ColumnField columnField = metamodel.getColumns().get(columnIndex);
                Class<?> fieldType = columnField.getType();
                Field field = columnField.getField();
                field.setAccessible(true);
                Object value = field.get(t);
                if(fieldType == int.class) {
                    statement.setInt(columnIndex + 2, (int) value);
                } else if (fieldType == String.class) {
                    statement.setString(columnIndex + 2, (String) value);
                } else if (fieldType == long.class) {
                    statement.setLong(columnIndex + 2, (long) value);
                }
            }

            return statement;
        }

        public PreparedStatement andParametersForUpdate(T t) throws SQLException, IllegalAccessException {
            Metamodel metamodel = Metamodel.of(t.getClass());

            for (int columnIndex = 0; columnIndex < metamodel.getColumns().size(); columnIndex ++) {
                ColumnField columnField = metamodel.getColumns().get(columnIndex);
                Class<?> fieldType = columnField.getType();
                Field field = columnField.getField();
                field.setAccessible(true);
                Object value = field.get(t);
                if(fieldType == int.class) {
                    statement.setInt(columnIndex + 1, (int) value);
                } else if (fieldType == String.class) {
                    statement.setString(columnIndex + 1, (String) value);
                } else if (fieldType == long.class) {
                    statement.setLong(columnIndex + 1, (long) value);
                }
            }

            setPrimaryKeyOnStatement(metamodel.getColumns().size() + 1, metamodel.getPrimaryKey().getValue(t));

            return statement;
        }

        public PreparedStatement andPrimaryKey(Object primaryKey) throws SQLException {
            setPrimaryKeyOnStatement(1, primaryKey);
            return statement;
        }

        private void setPrimaryKeyOnStatement(int position, Object primaryKey) throws SQLException {
            switch (primaryKey) {
                case Long key -> statement.setLong(position, key);
                case String key -> statement.setString(position, key);
                default -> throw new IllegalStateException("Type not supported for primary key: " + primaryKey.getClass());
            }
        }
    }

    private T buildInstanceFrom(Class<T> clss, ResultSet resultSet) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, SQLException {
        Metamodel metamodel = Metamodel.of(clss);
        T t = clss.getConstructor().newInstance();

        Field primaryKeyColumnField = metamodel.getPrimaryKey().getFiled();
        String primaryKeyColumnName = metamodel.getPrimaryKey().getName();
        Class<?> primaryKeyType = primaryKeyColumnField.getType();
        primaryKeyColumnField.setAccessible(true);

        if (primaryKeyType == long.class) {
            long primaryKeyValue = resultSet.getInt(primaryKeyColumnName);
            primaryKeyColumnField.set(t, primaryKeyValue);
        } else if (primaryKeyType == String.class) {
            String primaryKeyValue = resultSet.getString(primaryKeyColumnName);
            primaryKeyColumnField.set(t, primaryKeyValue);
        }

        for (ColumnField columnField : metamodel.getColumns()) {
            Field field = columnField.getField();
            field.setAccessible(true);
            String columnName = columnField.getName();
            Class<?> columnType = field.getType();
            if(columnType == int.class) {
                int columnValue = resultSet.getInt(columnName);
                field.set(t, columnValue);
            } else if(columnType == String.class) {
                String columnValue = resultSet.getString(columnName);
                field.set(t, columnValue);
            } else if (columnType == long.class) {
                long columValue = resultSet.getLong(columnName);
                field.set(t, columValue);
            }

        }
        return t;
    }
}
