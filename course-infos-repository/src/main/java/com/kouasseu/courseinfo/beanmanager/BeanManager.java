package com.kouasseu.courseinfo.beanmanager;

import com.kouasseu.courseinfo.annotations.Inject;
import com.kouasseu.courseinfo.annotations.Provides;
import com.kouasseu.courseinfo.provider.PostgresConnectionProvider;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  29/05/2023 -- 22:54<br></br>
 * By : @author alexk<br></br>
 * Project : Java Tips from 8 to 20<br></br>
 * Package : com.kouasseu.java8.orm_with_reflection_and_dinjection.beanmanager<br></br>
 */
public class BeanManager {
    private static final BeanManager instance = new BeanManager();
    private final Map<Class<?>, Supplier<?>> registry = new HashMap<>();

    public static BeanManager getInstance() {
        return instance;
    }

    private BeanManager() {
        List<Class<?>> classes = List.of(PostgresConnectionProvider.class); //Class to inject
        for (Class<?> clss: classes) {
            Method[] methods = clss.getDeclaredMethods();
            for(Method method: methods) {
                Provides provides = method.getAnnotation(Provides.class);
                if (provides != null) {
                    Class<?> returnType = method.getReturnType();
                    Supplier<?> supplier = () -> {
                        try {
                            if(!Modifier.isStatic(method.getModifiers())) {
                                Object o = clss.getConstructor().newInstance();
                                return method.invoke(o);
                            } else {
                                return method.invoke(null);
                            }
                        } catch (Exception e) {
                            throw new RuntimeException(e);
                        }
                    };
                    registry.put(returnType, supplier);
                }
            }
        }
    }

    public <T> T getInstance(Class<T> clss) {
        try {
            T t = clss.getConstructor().newInstance();
            Field[] fields = clss.getDeclaredFields();
            for(Field field : fields) {
                Inject inject = field.getAnnotation(Inject.class);
                if (inject != null) {
                    Class<?> injectedFieldType = field.getType();
                    Supplier<?> supplier = registry.get(injectedFieldType);
                    Object objectToInject = supplier.get();
                    field.setAccessible(true);
                    field.set(t, objectToInject);
                }
            }
            return t;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
