package com.kouasseu.courseinfo.domain;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  09/07/2023 -- 19:12<br></br>
 * By : @author alexk<br></br>
 * Project : course-info<br></br>
 * Package : com.kouasseu.courseinfo.domain<br></br>
 */
@DisplayName("Course Should")
class CourseTest {

    @DisplayName("throw an illegal argument exception if initialized with blank value on id, name or url field")
    @Test
    void thrownIllegalArgumentExceptionIfInitializationWithBlankString() {
        Exception ex = assertThrows(IllegalArgumentException.class, () -> new Courses("", "", 0, "", ""));
        assertEquals("No value present !", ex.getMessage());
    }


    @DisplayName("not throw an illegal argument exception if initialized with value in id, name or url field")
    @Test
    void NotThrownIllegalArgumentExceptionIfProperInitialize() {
        Courses courses = assertDoesNotThrow(() -> new Courses("id", "Course Name", 50, "2020-03-17T00:00:00+00:00", "https://app.pluralsight.com/library/courses/java-15-whats-new"));
        assertEquals("id", courses.id());
        assertEquals("Course Name", courses.name());
        assertEquals("2020-03-17T00:00:00+00:00", courses.displayDate());
        assertEquals(50, courses.length());
        assertTrue(courses.url().startsWith("https://app.pluralsight.com"));
    }

}